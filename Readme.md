    Kevin Jiang
    Network Security
    Summer 2014
    Final Project

# Secure Chat Service

## User credentials
* kevin:northeastern
* guest:password
* sam:iphone
* bob:mustang
* admin:mko0nji9

## Requirements
* Java 7 JDK
* Maven
* Connectivity to public maven repository

## Build instructions
* Make sure JAVA_HOME env variable points to JDK 1.7
* `cd` to the netsec_final directory and run `mvn clean install`

## Configuration
* The client reads configuration in from `client.properties`
* The server reads configuration in from `server.properties`
* These files can be edited to modify the server IP/port and other options

## Run instructions
* Run `./startServer.sh` to start the server
* Run `./startClient.sh` to start an instance of the client

## Third party libraries
Maven will take care of downloading all dependencies for the build. For runtime, the BC jar is included as part of this
package

* JLine - This library provides support for a console based application 
* Kryonet - This is a high level networking library that handles socket communication and object serialization. 
    I chose this library so that I could focus more time on the security aspects of the project, rather
    than networking details and serializing objects.
* Bouncy Castle - JCE provider