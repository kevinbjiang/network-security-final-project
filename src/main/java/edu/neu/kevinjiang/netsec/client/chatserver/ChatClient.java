package edu.neu.kevinjiang.netsec.client.chatserver;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import edu.neu.kevinjiang.netsec.client.App;
import edu.neu.kevinjiang.netsec.core.SymmetricCryptoEncryptResult;
import edu.neu.kevinjiang.netsec.core.protocol.MessageEnvelope;
import edu.neu.kevinjiang.netsec.core.protocol.login.LoginHello;
import edu.neu.kevinjiang.netsec.core.protocol.login.LoginHelloResponse;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import java.io.IOException;
import java.io.Serializable;
import java.net.ConnectException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidParameterSpecException;

/**
 * Created by kevinbjiang on 7/29/14.
 */
public class ChatClient
{
    private Client client;
    private App app;

    public ChatClient(App app)
    {
        this.app = app;
        this.client = new Client();

        Kryo kryo = this.client.getKryo();
        kryo.register(MessageEnvelope.class);
        kryo.register(LoginHello.class);
        kryo.register(LoginHelloResponse.class);
        kryo.register(byte[].class);
        kryo.register(org.bouncycastle.jce.provider.JCEDHPublicKey.class);
        kryo.register(javax.crypto.spec.DHParameterSpec.class);
        kryo.register(java.math.BigInteger.class);
        kryo.register(SymmetricCryptoEncryptResult.class);
    }

    public void connect(String ip, int port)
    {
        System.out.println("Connecting to " + ip + ":" + port);

        client.start();
        client.addListener(new ChatClientListener(app));
        try
        {
            client.connect(5000, ip, port);
        }
        catch (IOException e)
        {
            System.out.println("Server unreachable. Exiting");
            System.exit(-1);
        }
    }

    public void send(Serializable message, boolean encrypt, SecretKey key)
    {
        //System.out.println("Sending message: " + message.getClass().getName());

        try
        {
            MessageEnvelope envelope = new MessageEnvelope(message, key);
            client.sendTCP(envelope);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (NoSuchPaddingException e)
        {
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        catch (IllegalBlockSizeException e)
        {
            e.printStackTrace();
        }
        catch (BadPaddingException e)
        {
            e.printStackTrace();
        }
        catch (InvalidKeyException e)
        {
            e.printStackTrace();
        }
        catch (InvalidParameterSpecException e)
        {
            e.printStackTrace();
        }
    }

    public void close()
    {
        System.out.println("Closing client connection");
        client.close();
    }
}
