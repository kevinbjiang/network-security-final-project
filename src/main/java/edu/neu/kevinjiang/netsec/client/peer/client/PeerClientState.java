package edu.neu.kevinjiang.netsec.client.peer.client;

/**
 * Created by kevinbjiang on 8/2/14.
 */
public enum PeerClientState
{
    DISCONNECTED,
    SENT_DH_PUBLIC,
    AUTHENTICATED
}
