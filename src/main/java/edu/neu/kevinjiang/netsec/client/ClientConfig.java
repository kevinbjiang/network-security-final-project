package edu.neu.kevinjiang.netsec.client;

import edu.neu.kevinjiang.netsec.core.PropertiesReader;

/**
 * Created by kevinbjiang on 8/1/14.
 */
public class ClientConfig
{
    static final private ClientConfig _instance = new ClientConfig();

    static final public ClientConfig getInstance()
    {
        return _instance;
    }

    private PropertiesReader propertiesReader;

    public ClientConfig()
    {
        //read from properties
        propertiesReader = new PropertiesReader("client.properties");

        serverPublicKeyPath = (String) propertiesReader.getProperties().getProperty("serverPublicKeyPath", "server.pem");
        serverIp = propertiesReader.getProperties().getProperty("serverIp", "127.0.0.1");
        serverPort = Integer.valueOf(propertiesReader.getProperties().getProperty("serverPort", "55443"));
        peerStartingPort = Integer.valueOf(propertiesReader.getProperties().getProperty("peerStartingPort", "55444"));
    }

    private String serverPublicKeyPath;
    private String serverIp;
    private int serverPort;
    private int peerStartingPort;

    public String getServerPublicKeyPath()
    {
        return serverPublicKeyPath;
    }

    public String getServerIp()
    {
        return serverIp;
    }

    public int getServerPort()
    {
        return serverPort;
    }

    public int getPeerStartingPort()
    {
        return peerStartingPort;
    }
}
