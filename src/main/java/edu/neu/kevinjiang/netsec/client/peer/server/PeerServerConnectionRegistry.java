package edu.neu.kevinjiang.netsec.client.peer.server;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by kevinbjiang on 8/2/14.
 */
public class PeerServerConnectionRegistry
{
    protected Map<Integer, PeerServerConnectionState> registry = new ConcurrentHashMap<>();

    public PeerServerConnectionRegistry()
    {}

    public PeerServerConnectionState get(Integer connectionId)
    {
        synchronized (this)
        {
            return registry.get(connectionId);
        }
    }

    public boolean add(PeerServerConnectionState state)
    {
        synchronized (this)
        {
            if (registry.containsKey(state.getConnectionId()))
            {
                return false;
            }

            registry.put(state.getConnectionId(), state);
            return true;
        }
    }

    public Set<String> getPeerUserList()
    {
        Set<String> userList = new HashSet<>();
        synchronized (this)
        {
            for (Map.Entry<Integer, PeerServerConnectionState> entry : registry.entrySet())
            {
                String user = entry.getValue().getPeerUser();
                userList.add(user);
            }

            return userList;
        }
    }

    public PeerServerConnectionState getSessionForPeerUser(String user)
    {
        synchronized (this)
        {
            for (Map.Entry<Integer, PeerServerConnectionState> entry : registry.entrySet())
            {
                if (user.equals(entry.getValue().getPeerUser()))
                {
                    return entry.getValue();
                }
            }

            return null;
        }
    }

    public void removeConnection(Integer connectionId)
    {
        synchronized (this)
        {
            registry.remove(connectionId);
        }
    }
}
