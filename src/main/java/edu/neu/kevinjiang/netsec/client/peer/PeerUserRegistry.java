package edu.neu.kevinjiang.netsec.client.peer;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by kevinbjiang on 8/2/14.
 */
public class PeerUserRegistry
{
    private static final PeerUserRegistry _registry = new PeerUserRegistry();
    public static final PeerUserRegistry getInstance()
    {
        return _registry;
    }

    private PeerUserRegistry() {}

    Map<String, PeerUser> registry = new ConcurrentHashMap<>();

    public PeerUser get(String user)
    {
        synchronized (this)
        {
            PeerUser peerUser = registry.get(user);
            if (peerUser != null)
            {
                registry.remove(user);
            }

            return peerUser;
        }
    }

    public void put(PeerUser peerUser)
    {
        synchronized (this)
        {
            registry.put(peerUser.getUsername(), peerUser);
        }
    }
}
