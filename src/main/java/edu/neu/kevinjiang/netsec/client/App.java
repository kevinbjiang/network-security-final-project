package edu.neu.kevinjiang.netsec.client;

import com.esotericsoftware.minlog.Log;
import edu.neu.kevinjiang.netsec.client.chatserver.ChatClientState;
import edu.neu.kevinjiang.netsec.client.chatserver.ChatClient;
import edu.neu.kevinjiang.netsec.client.peer.client.PeerClientManager;
import edu.neu.kevinjiang.netsec.client.peer.server.PeerServer;
import edu.neu.kevinjiang.netsec.core.PEMKeyPairReader;
import jline.console.ConsoleReader;

import java.io.IOException;
import java.security.*;


public class App
{
    private final static App _app = new App();
    public final static App getInstance()
    {
        return _app;
    }


    private ChatClientState chatClientState;
    private ChatClient client;

    private PeerClientManager peerClientManager;
    private PeerServer peerServer;

    private App()
    {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        Log.set(Log.LEVEL_NONE);

        PublicKey publicKey;
        PEMKeyPairReader pemKeyPairReader = new PEMKeyPairReader();
        try
        {
            publicKey = pemKeyPairReader.readPublicKey(ClientConfig.getInstance().getServerPublicKeyPath());
        }
        catch (IOException e)
        {
            System.out.println("Invalid public key file: " + e);
            return;
        }

        client = new ChatClient(this);
        this.chatClientState = new ChatClientState(client, publicKey);
    }

    public static void main(String[] args) throws IOException
    {
        App app = App.getInstance();

        ConsoleReader consoleReader = app.init();
        app.runLoop(consoleReader);
    }

    private void runLoop(ConsoleReader console) throws IOException
    {
        console.setPrompt("\nchat>");
        String line = console.readLine();
        while (line != null)
        {
            boolean commandProcessed = false;
            String[] fullCommand = line.trim().split(" ");
            if (fullCommand.length > 0)
            {
                commandProcessed = true;
                String command = fullCommand[0];
                if (command.equalsIgnoreCase("list"))
                {
                    chatClientState.sendListRequest();
                }
                else if (command.equalsIgnoreCase("ping"))
                {
                    chatClientState.sendHeartBeat();

                }
                else if (command.startsWith("send"))
                {
                    if (fullCommand.length <= 2)
                    {
                        console.print("Usage: send <username>");
                    }
                    else
                    {
                        String user = fullCommand[1];

                        StringBuilder builder = new StringBuilder();
                        for (int i = 2 ; i < fullCommand.length; ++i)
                        {
                            builder.append(fullCommand[i]).append(" ");
                        }
                        String message = builder.toString();

                        this.peerClientManager.sendMessageToUser(user, message);
                    }
                }
                else if (command.equalsIgnoreCase("help"))
                {
                    console.print("Available commands:\n\n");
                    console.print("list\n");
                    console.print("send <username> <message>\n");
                    console.print("quit\n");
                }
                else if (command.equalsIgnoreCase("quit"))
                {
                    System.exit(0);
                    return;
                }
                else
                {
                    commandProcessed = false;
                }
            }

            if (!commandProcessed)
            {
                console.print("Invalid command: " + line + "\n");
            }

            line = console.readLine();
        }
    }

    public ConsoleReader init() throws IOException
    {
        ConsoleReader console = new ConsoleReader();

        //TODO prompt for user credentials
        String username = console.readLine("\nUsername: ");
        String password = console.readLine("\nPassword: ", '*');

        //start peer server
        peerServer = new PeerServer(ClientConfig.getInstance().getPeerStartingPort());

        //create client
        PrivateKey selfPrivateKey = chatClientState.loginSynchronous(username, password, peerServer.getPort());

        //initialize peer client
        peerClientManager = new PeerClientManager(selfPrivateKey);

        //set private key
        peerServer.setPrivateKey(selfPrivateKey);

        return console;
    }

    public ChatClientState getChatClientState()
    {
        return chatClientState;
    }

    public PeerClientManager getPeerClientManager()
    {
        return peerClientManager;
    }
}
