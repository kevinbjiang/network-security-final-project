package edu.neu.kevinjiang.netsec.client.chatserver;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage;
import com.esotericsoftware.kryonet.Listener;
import edu.neu.kevinjiang.netsec.client.App;
import edu.neu.kevinjiang.netsec.core.protocol.MessageEnvelope;
import edu.neu.kevinjiang.netsec.core.protocol.login.LoginAuthenticateResponse;
import edu.neu.kevinjiang.netsec.core.protocol.login.LoginHelloResponse;
import edu.neu.kevinjiang.netsec.core.protocol.session.ClientInfoResponse;
import edu.neu.kevinjiang.netsec.core.protocol.session.HeartbeatResponse;
import edu.neu.kevinjiang.netsec.core.protocol.session.ListResponse;

import javax.crypto.SecretKey;

/**
 * Created by kevinbjiang on 7/29/14.
 */
public class ChatClientListener extends Listener
{
    private App app = null;

    public ChatClientListener(App app)
    {
        this.app = app;
    }

    @Override
    public void connected(Connection connection)
    {
        super.connected(connection);

        //System.out.println("Connected to server: " + connection.getRemoteAddressTCP());
    }

    @Override
    public void disconnected(Connection connection)
    {
        super.disconnected(connection);

        System.out.println("Lost connection with server.. ending session");
        System.exit(-1);

        //System.out.println("Disconnected from server: " + connection.getRemoteAddressTCP());
    }

    @Override
    public void received(Connection connection, Object object)
    {
        super.received(connection, object);

        if (object instanceof FrameworkMessage)
        {
            return;
        }

        if (!(object instanceof MessageEnvelope))
        {
            System.out.println("Invalid message format received");
            return;
        }

        Object message = null;
        MessageEnvelope envelope = (MessageEnvelope)object;
        if (envelope.encrypted)
        {
            SecretKey aesKey = app.getChatClientState().getAesKey();
            message = envelope.read(aesKey);
        }
        else
        {
            message = envelope.read(null);
        }

        //System.out.println("Received message: " + message.getClass().getName());

        if (message instanceof LoginAuthenticateResponse)
        {
            app.getChatClientState().receiveLoginAuthenticateResponse((LoginAuthenticateResponse) message);
        }
        else if (message instanceof LoginHelloResponse)
        {
            app.getChatClientState().receiveLoginHelloResponse((LoginHelloResponse) message);
        }
        else if (message instanceof ClientInfoResponse)
        {
            app.getChatClientState().receiveClientInfoResponse((ClientInfoResponse)message);
        }
        else if (message instanceof HeartbeatResponse)
        {
            app.getChatClientState().receiveHeartbeatResponse((HeartbeatResponse) message);
        }
        else if (message instanceof ListResponse)
        {
            app.getChatClientState().receiveListResponse((ListResponse) message);
        }
        else
        {
            System.out.println("Dropping unexpected protocol message received from server: " + message.getClass().getName());
        }
    }
}
