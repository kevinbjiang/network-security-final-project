package edu.neu.kevinjiang.netsec.client.peer.client;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import edu.neu.kevinjiang.netsec.client.App;
import edu.neu.kevinjiang.netsec.core.*;
import edu.neu.kevinjiang.netsec.core.protocol.MessageEnvelope;
import edu.neu.kevinjiang.netsec.core.protocol.peer.PeerHello;
import edu.neu.kevinjiang.netsec.core.protocol.peer.PeerHelloResponse;
import edu.neu.kevinjiang.netsec.core.protocol.peer.PeerMessage;

import javax.crypto.SecretKey;
import java.io.IOException;
import java.io.Serializable;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;

/**
 * Created by kevinbjiang on 7/26/14.
 */
public class PeerClient
{
    private static final SecureRandom random = new SecureRandom();

    final private Client client;
    final private PeerClientConnectionState state = new PeerClientConnectionState();

    public PeerClient(PrivateKey selfPrivateKey, String peerUsername, String ip, int port, PublicKey peerPublicKey) throws IOException
    {
        state.setSelfPrivateKey(selfPrivateKey);
        state.setPeerPublicKey(peerPublicKey);
        state.setAddress(ip);
        state.setPort(port);
        state.setPeerUser(peerUsername);

        this.client = new Client();

        Kryo kryo = this.client.getKryo();
        kryo.register(MessageEnvelope.class);
        kryo.register(PeerHello.class);
        kryo.register(PeerHelloResponse.class);
        kryo.register(byte[].class);
        kryo.register(org.bouncycastle.jce.provider.JCEDHPublicKey.class);
        kryo.register(javax.crypto.spec.DHParameterSpec.class);
        kryo.register(java.math.BigInteger.class);
        kryo.register(SymmetricCryptoEncryptResult.class);

        client.start();
        client.addListener(new PeerClientConnectionListener(state, this));
        client.connect(10000, ip, port);
    }

    protected void send(Serializable message, SecretKey key)
    {
        //System.out.println("Sending message: " + message.getClass().getName());

        try
        {
            MessageEnvelope envelope = new MessageEnvelope(message, key);
            client.sendTCP(envelope);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void close()
    {
        //System.out.println("Closing client connection");
        client.close();
    }

    public void sendPeerHello(String initialMessage) throws IOException
    {
        synchronized (state)
        {
            if (state.getState() != PeerClientState.DISCONNECTED)
            {
                return;
            }

            //save initial message to state
            state.setInitialMessage(initialMessage);

            //init dh
            DiffieHellmanUtil diffieHellmanUtil = state.getDiffieHellmanUtil();
            PublicKey dhPublicKey = diffieHellmanUtil.init();

            //generate nonce
            byte[] challengeNonce = generateChallengeNonce();
            state.setSentChallengeNonce(challengeNonce);

            //prepare request
            String username = App.getInstance().getChatClientState().getUsername();
            byte[] initiatorDHPublicKey = diffieHellmanUtil.serializeDhPublicKey(dhPublicKey);

            //create and encrypt
            PeerHello peerHello = new PeerHello(username, challengeNonce, initiatorDHPublicKey);
            peerHello.encrypt(state.getPeerPublicKey());

            //send unencrypted (no aes key yet)
            send(peerHello, null);

            //transition
            state.setState(PeerClientState.SENT_DH_PUBLIC);
        }
    }

    public void sendPeerMessage(String message)
    {
        synchronized (state)
        {
            if (state.getState() != PeerClientState.AUTHENTICATED)
            {
                return;
            }

            //build message request and increment counter
            long messageid = state.getLastSentMessageId();
            state.setLastSentMessageId(messageid + 1);

            //create message
            PeerMessage peerMessage = new PeerMessage(messageid, message);

            //send message
            send(peerMessage, state.getAesKey());
        }
    }

    private byte[] generateChallengeNonce()
    {
        //generate challenge nonce
        byte[] challengeNonce = new byte[32];
        random.nextBytes(challengeNonce);
        return challengeNonce;
    }
}
