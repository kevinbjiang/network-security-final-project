package edu.neu.kevinjiang.netsec.client.peer.server;

/**
 * Created by kevinbjiang on 8/2/14.
 */
public enum PeerServerState
{
    DISCONNECTED,
    HANDSHAKE,
    AUTHENTICATED
}
