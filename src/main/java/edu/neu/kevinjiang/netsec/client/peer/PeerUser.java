package edu.neu.kevinjiang.netsec.client.peer;

import java.security.PublicKey;

/**
 * Created by kevinbjiang on 8/2/14.
 */
public class PeerUser
{
    public String username;
    public String address;
    public int port;
    public PublicKey clientPublicKey;
    boolean online = true;

    public PeerUser()
    {

    }

    public PeerUser(String username, String address, int port, PublicKey clientPublicKey)
    {
        this.username = username;
        this.address = address;
        this.port = port;
        this.clientPublicKey = clientPublicKey;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getAddress()
    {
        return address;
    }

    public int getPort()
    {
        return port;
    }

    public PublicKey getClientPublicKey()
    {
        return clientPublicKey;
    }

    public boolean isOnline()
    {
        return online;
    }

    public void setOnline(boolean online)
    {
        this.online = online;
    }
}
