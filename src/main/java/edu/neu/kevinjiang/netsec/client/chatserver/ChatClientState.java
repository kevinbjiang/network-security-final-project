package edu.neu.kevinjiang.netsec.client.chatserver;

import edu.neu.kevinjiang.netsec.client.ClientConfig;
import edu.neu.kevinjiang.netsec.client.peer.PeerUser;
import edu.neu.kevinjiang.netsec.client.peer.PeerUserRegistry;
import edu.neu.kevinjiang.netsec.core.*;
import edu.neu.kevinjiang.netsec.core.protocol.login.LoginAuthenticate;
import edu.neu.kevinjiang.netsec.core.protocol.login.LoginAuthenticateResponse;
import edu.neu.kevinjiang.netsec.core.protocol.login.LoginHello;
import edu.neu.kevinjiang.netsec.core.protocol.login.LoginHelloResponse;
import edu.neu.kevinjiang.netsec.core.protocol.session.*;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by kevinbjiang on 7/29/14.
 */
public class ChatClientState
{
    private static final SecureRandom random = new SecureRandom();

    private SymmetricCryptoHelper symmetricCryptoHelper = new SymmetricCryptoHelper();
    private AsymmetricCryptoHelper asymmetricCryptoHelper = new AsymmetricCryptoHelper();
    private DigestUtil digestUtil = new DigestUtil();
    private PBKDFCryptoHelper pbkdfCryptoHelper = new PBKDFCryptoHelper();
    private JavaSerializer javaSerializer = new JavaSerializer();

    private ChatClient client;
    private DiffieHellmanUtil diffieHellmanUtil = new DiffieHellmanUtil();
    private PublicKey selfDHPublicKey;
    private PublicKey serverPublicKey;
    private PrivateKey selfPrivateKey;
    private int peerServerPort;

    private ChatClientStateEnum applicationState = ChatClientStateEnum.DISCONNECTED;
    private long lastHeartbeat;
    private SecretKey aesKey = null;
    private byte[] challengeNonce = new byte[32];
    private String username;
    private String password;
    private long messageIdCounter = 0;

    private Set<Long> messagesWaitingForResponse = new HashSet<Long>();

    public ChatClientState(ChatClient client, PublicKey serverPublicKey)
    {
        this.client = client;
        this.serverPublicKey = serverPublicKey;
        selfDHPublicKey = diffieHellmanUtil.init();
    }

    public PrivateKey loginSynchronous(String username, String password, int peerServerPort) throws IOException
    {
        synchronized (this)
        {
            //start off handshake chain
            login(username, password, peerServerPort);

            //wait for response
            while (applicationState != ChatClientStateEnum.AUTHENTICATED)
            {
                try
                {
                    this.wait(10000);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }

            return selfPrivateKey;
        }
    }


    public void login(String username, String password, int peerServerPort) throws IOException
    {
        synchronized (this)
        {
            if (applicationState != ChatClientStateEnum.DISCONNECTED)
            {
                throw new IllegalStateException("login when not DISCONNECTED");
            }

            //save username/password for later
            this.username = username;
            this.password = password;
            this.peerServerPort = peerServerPort;

            //create login hello and save challenge nonce
            random.nextBytes(challengeNonce);
            LoginHello loginHello = new LoginHello();
            loginHello.challengeNonce = challengeNonce;


            loginHello.dhClientPublicKey = javaSerializer.serialize(selfDHPublicKey);

            //send login hello
            this.client.connect(ClientConfig.getInstance().getServerIp(), ClientConfig.getInstance().getServerPort());
            client.send(loginHello, false, null);

            //transition
            applicationState = ChatClientStateEnum.WAITING_HELLO_RESPONSE;
        }
    }

    public void receiveLoginHelloResponse(LoginHelloResponse response)
    {
        synchronized (this)
        {
            if (applicationState != ChatClientStateEnum.WAITING_HELLO_RESPONSE)
            {
                return;
            }

            //validate challenge nonce response with server pubkey, otherwise fail
            boolean valid = asymmetricCryptoHelper.validateSignature(serverPublicKey, challengeNonce, response.challengeNonceResponse);
            if (!valid)
            {
                System.out.println("Server failed challenge! Possible MITM attempt!");
                return;
            }

            //derive dh session key
            PublicKey dhServerPublicKey;
            try
            {
                dhServerPublicKey = (PublicKey) javaSerializer.deserialize(response.dhServerPublicKey);
            }
            catch (ClassCastException e)
            {
                System.out.println("Unexpected class received in place of dh public key");
                return;
            }

            byte[] dhSessionKey = diffieHellmanUtil.deriveDHKey(dhServerPublicKey);

            //derive aes key from dh session key
            byte[] sha1DiffieKey = digestUtil.sha1(dhSessionKey);
            byte[] aesKeyBytes = Arrays.copyOfRange(sha1DiffieKey, 0, 16);
            aesKey = new SecretKeySpec(aesKeyBytes, 0, aesKeyBytes.length, "AES");

            //prepare response
            LoginAuthenticate loginAuthenticate = new LoginAuthenticate();
            loginAuthenticate.username = this.username;
            loginAuthenticate.password = this.password;
            loginAuthenticate.port = this.peerServerPort;

            //send encrypted loginAuthenticate
            client.send(loginAuthenticate, true, aesKey);

            //transition
            applicationState = ChatClientStateEnum.WAITING_AUTH_RESPONSE;
        }
    }

    public void receiveLoginAuthenticateResponse(LoginAuthenticateResponse response)
    {
        synchronized (this)
        {
            if (applicationState != ChatClientStateEnum.WAITING_AUTH_RESPONSE)
            {
                return;
            }

            if (!response.authenticated)
            {
                System.out.println("Password authentication failed");
                System.exit(-1);
                return;
            }

            //store away private key for communication with peers
            this.selfPrivateKey = response.sessionPrivateKey;

            //transition
            applicationState = ChatClientStateEnum.AUTHENTICATED;
            this.notifyAll();
        }
    }

    public void receiveHeartbeatResponse(HeartbeatResponse response)
    {
        synchronized (this)
        {
            if (applicationState != ChatClientStateEnum.AUTHENTICATED)
            {
                return;
            }

            //replay protection
            Long inResponseTo = response.inResponseTo;
            if (!messagesWaitingForResponse.contains(inResponseTo))
            {
                System.out.println("Response to unexpected message id. Attempted replay attack?");
                return;
            }
            messagesWaitingForResponse.remove(inResponseTo);

            //handle response
            //store heartbeat timestamp
            this.lastHeartbeat = System.currentTimeMillis();
        }
    }

    public void receiveListResponse(ListResponse response)
    {
        synchronized (this)
        {
            if (applicationState != ChatClientStateEnum.AUTHENTICATED)
            {
                return;
            }

            //replay protection
            Long inResponseTo = response.inResponseTo;
            if (!messagesWaitingForResponse.contains(inResponseTo))
            {
                System.out.println("Response to unexpected message id. Attempted replay attack?");
                return;
            }
            messagesWaitingForResponse.remove(inResponseTo);

            //handle response
            //print list of users to console
            StringBuilder builder = new StringBuilder("Users online: \n");
            for (String user : response.users)
            {
                builder.append(user).append("\n");
            }
            builder.append("\n");

            System.out.println(builder.toString());
        }
    }

    public void receiveClientInfoResponse(ClientInfoResponse response)
    {
        synchronized (this)
        {
            if (applicationState != ChatClientStateEnum.AUTHENTICATED)
            {
                return;
            }

            //replay protection
            Long inResponseTo = response.inResponseTo;
            if (!messagesWaitingForResponse.contains(inResponseTo))
            {
                System.out.println("Response to unexpected message id. Attempted replay attack?");
                return;
            }
            messagesWaitingForResponse.remove(inResponseTo);

            //add new info to user registry
            PeerUserRegistry peerUserRegistry = PeerUserRegistry.getInstance();
            PeerUser peerUser = new PeerUser(response.username, response.address, response.port, response.clientPublicKey);
            peerUser.setOnline(response.success);
            peerUserRegistry.put(peerUser);

            //signal waiting clients
            synchronized (peerUserRegistry)
            {
                peerUserRegistry.notifyAll();
            }
        }
    }

    public void sendHeartBeat()
    {
        synchronized (this)
        {
            if (applicationState != ChatClientStateEnum.AUTHENTICATED)
            {
                System.out.println("Cannot send heartbeat. Not yet authenticated");
                return;
            }

            //incr counter, store request in req map
            ++messageIdCounter;
            messagesWaitingForResponse.add(messageIdCounter);

            //create request and send
            HeartbeatRequest heartbeatRequest = new HeartbeatRequest();
            heartbeatRequest.messageId = messageIdCounter;

            client.send(heartbeatRequest, true, aesKey);
        }
    }

    public void sendListRequest()
    {
        synchronized (this)
        {
            if (applicationState != ChatClientStateEnum.AUTHENTICATED)
            {
                System.out.println("Cannot send heartbeat. Not yet authenticated");
                return;
            }

            //incr counter, store request in req map
            ++messageIdCounter;
            messagesWaitingForResponse.add(messageIdCounter);

            //create request and send
            ListRequest listRequest = new ListRequest();
            listRequest.messageId = messageIdCounter;

            client.send(listRequest, true, aesKey);
        }
    }

    public void sendClientInfoRequest(String user)
    {
        synchronized (this)
        {
            if (applicationState != ChatClientStateEnum.AUTHENTICATED)
            {
                System.out.println("Cannot send heartbeat. Not yet authenticated");
                return;
            }

            //incr counter, store request in req map
            ++messageIdCounter;
            messagesWaitingForResponse.add(messageIdCounter);

            //create request and send
            ClientInfoRequest clientInfoRequest = new ClientInfoRequest();
            clientInfoRequest.messageId = messageIdCounter;
            clientInfoRequest.username = user;

            client.send(clientInfoRequest, true, aesKey);
        }
    }

    /**
     * Synchronous call to request and return peer user data from the chat server
     *
     * @param peerUsername
     * @return
     */
    public PeerUser retrieveClientInfoSynchronous(String peerUsername)
    {
        //send client info request
        sendClientInfoRequest(peerUsername);

        //wait for client info response
        PeerUserRegistry peerUserRegistry = PeerUserRegistry.getInstance();
        PeerUser userRegData = peerUserRegistry.get(peerUsername);

        while (userRegData == null)
        {
            try
            {
                //wait for notification
                synchronized (peerUserRegistry)
                {
                    peerUserRegistry.wait(10000);
                }

                //wake up and check
                userRegData = peerUserRegistry.get(peerUsername);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }

        //handle case when user is not online
        if (!userRegData.isOnline())
        {
            return null;
        }

        //return response data
        return userRegData;
    }


    public SecretKey getAesKey()
    {
        return aesKey;
    }

    public PrivateKey getSelfPrivateKey()
    {
        return selfPrivateKey;
    }

    public String getUsername()
    {
        return username;
    }
}
