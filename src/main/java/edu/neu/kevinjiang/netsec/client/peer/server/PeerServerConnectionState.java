package edu.neu.kevinjiang.netsec.client.peer.server;

import edu.neu.kevinjiang.netsec.client.peer.client.PeerClientState;
import edu.neu.kevinjiang.netsec.core.DiffieHellmanUtil;


import javax.crypto.SecretKey;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * Created by kevinbjiang on 8/2/14.
 */
public class PeerServerConnectionState
{
    //initialized on creation
    private final DiffieHellmanUtil diffieHellmanUtil = new DiffieHellmanUtil();
    private PeerServerState state = PeerServerState.DISCONNECTED;
    private long lastReceivedMessageId = 0;

    final private PrivateKey selfPrivateKey;
    final private Integer connectionId;

    //set as handshake occurs
    private String peerUser;
    private PublicKey peerPublicKey;
    private byte[] sentChallengeNonce;
    private SecretKey aesKey;

    public PeerServerConnectionState(PrivateKey selfPrivateKey, Integer connectionId)
    {
        this.selfPrivateKey = selfPrivateKey;
        this.connectionId = connectionId;
    }

    public SecretKey getAesKey()
    {
        return aesKey;
    }

    public void setAesKey(SecretKey aesKey)
    {
        this.aesKey = aesKey;
    }

    public byte[] getSentChallengeNonce()
    {
        return sentChallengeNonce;
    }

    public void setSentChallengeNonce(byte[] sentChallengeNonce)
    {
        this.sentChallengeNonce = sentChallengeNonce;
    }

    public PublicKey getPeerPublicKey()
    {
        return peerPublicKey;
    }

    public void setPeerPublicKey(PublicKey peerPublicKey)
    {
        this.peerPublicKey = peerPublicKey;
    }

    public String getPeerUser()
    {
        return peerUser;
    }

    public void setPeerUser(String peerUser)
    {
        this.peerUser = peerUser;
    }

    public Integer getConnectionId()
    {
        return connectionId;
    }

    public PrivateKey getSelfPrivateKey()
    {
        return selfPrivateKey;
    }

    public long getLastReceivedMessageId()
    {
        return lastReceivedMessageId;
    }

    public void setLastReceivedMessageId(long lastReceivedMessageId)
    {
        this.lastReceivedMessageId = lastReceivedMessageId;
    }

    public PeerServerState getState()
    {
        return state;
    }

    public void setState(PeerServerState state)
    {
        this.state = state;
    }

    public DiffieHellmanUtil getDiffieHellmanUtil()
    {
        return diffieHellmanUtil;
    }
}
