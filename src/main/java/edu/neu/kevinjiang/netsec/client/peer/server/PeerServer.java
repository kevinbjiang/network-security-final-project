package edu.neu.kevinjiang.netsec.client.peer.server;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Server;
import edu.neu.kevinjiang.netsec.client.App;
import edu.neu.kevinjiang.netsec.client.chatserver.ChatClientState;
import edu.neu.kevinjiang.netsec.core.SymmetricCryptoEncryptResult;
import edu.neu.kevinjiang.netsec.core.protocol.MessageEnvelope;
import edu.neu.kevinjiang.netsec.core.protocol.peer.PeerHello;
import edu.neu.kevinjiang.netsec.core.protocol.peer.PeerHelloResponse;

import javax.crypto.SecretKey;
import java.io.IOException;
import java.io.Serializable;
import java.security.PrivateKey;


/**
 * Created by kevinbjiang on 7/26/14.
 */
public class PeerServer
{
    private Server server;
    private PrivateKey privateKey;
    private PeerServerConnectionRegistry registry = new PeerServerConnectionRegistry();
    private ChatClientState chatClientState = App.getInstance().getChatClientState();
    private int port;


    public PeerServer(int startingPort)
    {
        this.server = new Server();

        Kryo kryo = this.server.getKryo();
        kryo.register(MessageEnvelope.class);
        kryo.register(PeerHello.class);
        kryo.register(PeerHelloResponse.class);
        kryo.register(byte[].class);
        kryo.register(org.bouncycastle.jce.provider.JCEDHPublicKey.class);
        kryo.register(javax.crypto.spec.DHParameterSpec.class);
        kryo.register(java.math.BigInteger.class);
        kryo.register(SymmetricCryptoEncryptResult.class);

        server.start();
        server.addListener(new PeerServerConnectionListener(this));

        //try ports
        port = startingPort;
        for (int i = 0; i < 5; ++i)
        {
            try
            {
                server.bind(port);
            }
            catch (IOException e)
            {
                port++;
                System.out.println("Couldn't bind to port " + port + ", trying next");
                if (i == 5)
                {
                    throw new RuntimeException("Unable to bind to any ports");
                }
                continue;
            }
            break;
        }

        System.out.println("Starting peer server on port " + port);
    }

    public void send(int connectionId, Serializable message, SecretKey key)
    {
        //System.out.println("Sending message: " + message.getClass().getName());

        try
        {
            MessageEnvelope envelope = new MessageEnvelope(message, key);
            server.sendToTCP(connectionId, envelope);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void close()
    {
        //System.out.println("Closing server connection");
        server.close();
    }

    public PrivateKey getPrivateKey()
    {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey)
    {
        this.privateKey = privateKey;
    }

    public PeerServerConnectionRegistry getRegistry()
    {
        return registry;
    }

    public ChatClientState getChatClientState()
    {
        return chatClientState;
    }

    public int getPort()
    {
        return port;
    }
}
