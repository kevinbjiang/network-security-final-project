package edu.neu.kevinjiang.netsec.client.peer.client;

import edu.neu.kevinjiang.netsec.client.App;
import edu.neu.kevinjiang.netsec.client.chatserver.ChatClientState;
import edu.neu.kevinjiang.netsec.client.peer.PeerUser;

import java.io.IOException;
import java.security.PrivateKey;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by kevinbjiang on 8/2/14.
 */
public class PeerClientManager
{
    protected ChatClientState chatClientState = App.getInstance().getChatClientState();

    protected Map<String, PeerClient> clientRegistry = new ConcurrentHashMap<>();
    final private PrivateKey selfPrivateKey;

    public PeerClientManager(PrivateKey selfPrivateKey)
    {
        this.selfPrivateKey = selfPrivateKey;
    }

    /**
     * If a connection with a user already exists, re-use it
     * Otherwise open a new connection and store it
     * Then send the message
     */
    public void sendMessageToUser(String peerUsername, String message) throws IOException
    {
        //check registry for existing client
        PeerClient peerClient = clientRegistry.get(peerUsername);

        //if no client yet
        if (peerClient == null)
        {
            //request peer user info from chat server
            PeerUser peerUserData = chatClientState.retrieveClientInfoSynchronous(peerUsername);
            if (peerUserData == null)
            {
                System.out.println("No such user is online");
                return;
            }

            //create client
            peerClient = new PeerClient(selfPrivateKey, peerUsername, peerUserData.getAddress(), peerUserData.getPort(), peerUserData.getClientPublicKey());
            peerClient.sendPeerHello(message);

            //add to registry
            clientRegistry.put(peerUsername, peerClient);
        }
        else
        {
            //send message using existing client
            peerClient.sendPeerMessage(message);
        }
    }

    public Map<String, PeerClient> getClientRegistry()
    {
        return clientRegistry;
    }
}
