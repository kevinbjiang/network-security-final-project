package edu.neu.kevinjiang.netsec.client.peer.server;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage;
import com.esotericsoftware.kryonet.Listener;
import edu.neu.kevinjiang.netsec.client.peer.PeerUser;
import edu.neu.kevinjiang.netsec.client.peer.client.PeerClientState;
import edu.neu.kevinjiang.netsec.core.DiffieHellmanUtil;

import edu.neu.kevinjiang.netsec.core.protocol.MessageEnvelope;
import edu.neu.kevinjiang.netsec.core.protocol.peer.PeerAuthenticate;
import edu.neu.kevinjiang.netsec.core.protocol.peer.PeerHello;
import edu.neu.kevinjiang.netsec.core.protocol.peer.PeerHelloResponse;
import edu.neu.kevinjiang.netsec.core.protocol.peer.PeerMessage;
import org.bouncycastle.jce.provider.JCEDHPublicKey;

import javax.crypto.SecretKey;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.util.Arrays;

/**
 * Created by kevinbjiang on 7/26/14.
 */
public class PeerServerConnectionListener extends Listener
{
    //utils
    private static final SecureRandom random = new SecureRandom();

    private final PeerServer server;
    private final PeerServerConnectionRegistry peerServerConnectionRegistry;

    public PeerServerConnectionListener(PeerServer server)
    {
        this.server = server;
        this.peerServerConnectionRegistry = server.getRegistry();
    }

    @Override
    public void connected(Connection connection)
    {
        super.connected(connection);
    }

    @Override
    public void disconnected(Connection connection)
    {
        super.disconnected(connection);

//        String peerUser = peerServerConnectionRegistry.get(connection.getID()).getPeerUser();
//        if (peerUser != null)
//        {
//            System.out.println("User [" + peerUser + "] disconnected");
//        }
//        else
//        {
//            System.out.println("Unauthenticated client disconnected");
//        }

        //remove connection from registry on disconnect
        peerServerConnectionRegistry.removeConnection(connection.getID());

    }

    @Override
    public void received(Connection connection, Object object)
    {
        if (object instanceof FrameworkMessage)
        {
            return;
        }

        if (!(object instanceof MessageEnvelope))
        {
            System.out.println("Invalid message format received");
            return;
        }

        //check if we have a connection state. if not, create one
        PeerServerConnectionState connectionState = peerServerConnectionRegistry.get(connection.getID());
        if (connectionState == null)
        {
            //create connection state
            connectionState = new PeerServerConnectionState(this.server.getPrivateKey(), connection.getID());
            peerServerConnectionRegistry.add(connectionState);
        }

        Object message = null;
        MessageEnvelope envelope = (MessageEnvelope)object;
        if (envelope.encrypted)
        {

            SecretKey aesKey = connectionState.getAesKey();
            if (aesKey == null)
            {
                System.out.println("Sender encrypted message before session key established. Dropping message");
                return;
            }

            message = envelope.read(aesKey);
        }
        else
        {
            message = envelope.read(null);
        }

        //System.out.println("Received message: " + message.getClass().getName());

        if (message instanceof PeerHello)
        {
            handlePeerHello(connectionState, (PeerHello) message);
        }
        else if (message instanceof PeerAuthenticate)
        {
            handlePeerAuthenticate(connectionState, (PeerAuthenticate) message);
        }
        else if (message instanceof PeerMessage)
        {
            handlePeerMessage(connectionState, (PeerMessage) message);
        }
        else
        {
            System.out.println("Dropping unexpected protocol message received from peer: " + message.getClass().getName());
        }
    }


    public void handlePeerHello(PeerServerConnectionState connectionState, PeerHello request)
    {
        synchronized (connectionState)
        {
            if (connectionState.getState() != PeerServerState.DISCONNECTED)
            {
                return;
            }

            //decrypt message
            request.decrypt(server.getPrivateKey());

            //make sure we don't already have a connection with this user
            String peerUsername = request.getUsername();
            if (peerServerConnectionRegistry.getPeerUserList().contains(peerUsername))
            {
                System.out.println("Closing connection. Already have connection open with user " + peerUsername);
                server.close();
                return;
            }

            //save peer username
            connectionState.setPeerUser(peerUsername);

            //look up initiator username's public key
            PeerUser peerUserData = server.getChatClientState().retrieveClientInfoSynchronous(peerUsername);

            //save client public key
            connectionState.setPeerPublicKey(peerUserData.getClientPublicKey());

            request.getInitiatorDHPublicKey();
            request.getInitiatorChallengeNonce();

            //derive diffie hellman key and session key
            DiffieHellmanUtil diffieHellmanUtil = connectionState.getDiffieHellmanUtil();
            JCEDHPublicKey jcedhPublicKey = diffieHellmanUtil.deserializeDHPublicKey(request.getInitiatorDHPublicKey());

            //generate public key
            PublicKey dhPublicKey = diffieHellmanUtil.init(jcedhPublicKey);
            byte[] dbPublicKeySerialized = diffieHellmanUtil.serializeDhPublicKey(dhPublicKey);

            //derive dh session key
            SecretKey aesKey = diffieHellmanUtil.deriveAESKeyFromPublicKeys(jcedhPublicKey);
            connectionState.setAesKey(aesKey);

            //generate challenge nonce
            byte[] challengeNonce = generateChallengeNonce();
            connectionState.setSentChallengeNonce(challengeNonce);

            //build response
            PeerHelloResponse peerHelloResponse = new PeerHelloResponse(challengeNonce, request.getInitiatorChallengeNonce(), dbPublicKeySerialized);
            peerHelloResponse.encrypt(connectionState.getPeerPublicKey());

            //send response
            server.send(connectionState.getConnectionId(), peerHelloResponse, null);

            //transition
            connectionState.setState(PeerServerState.HANDSHAKE);
        }
    }

    public void handlePeerAuthenticate(PeerServerConnectionState connectionState, PeerAuthenticate request)
    {
        synchronized (connectionState)
        {
            if (connectionState.getState() != PeerServerState.HANDSHAKE)
            {
                return;
            }

            //check challenge response
            if (!Arrays.equals(connectionState.getSentChallengeNonce(), request.getResponderChallengeNonceResponse()))
            {
                System.out.println("challenge failed");
                return;
            }

            //display message
            System.out.println(connectionState.getPeerUser() + ": " + request.getInitialMessage());

            //transition
            connectionState.setState(PeerServerState.AUTHENTICATED);
        }
    }

    public void handlePeerMessage(PeerServerConnectionState connectionState, PeerMessage request)
    {
        synchronized (connectionState)
        {
            if (connectionState.getState() != PeerServerState.AUTHENTICATED)
            {
                return;
            }

            //check message id and increment counter
            if (request.getMessageid() <= connectionState.getLastReceivedMessageId())
            {
                return;
            }
            connectionState.setLastReceivedMessageId(connectionState.getLastReceivedMessageId() + 1);

            //print message
            System.out.println(connectionState.getPeerUser() + ": " + request.getMessage());
        }
    }

    private byte[] generateChallengeNonce()
    {
        //generate challenge nonce
        byte[] challengeNonce = new byte[32];
        random.nextBytes(challengeNonce);
        return challengeNonce;
    }


}
