package edu.neu.kevinjiang.netsec.client.chatserver;

/**
 * Created by kevinbjiang on 7/29/14.
 */
public enum ChatClientStateEnum
{
    DISCONNECTED,
    WAITING_HELLO_RESPONSE,
    WAITING_AUTH_RESPONSE,
    AUTHENTICATED
}
