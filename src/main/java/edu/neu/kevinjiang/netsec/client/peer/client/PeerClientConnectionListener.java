package edu.neu.kevinjiang.netsec.client.peer.client;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage;
import com.esotericsoftware.kryonet.Listener;
import edu.neu.kevinjiang.netsec.client.App;
import edu.neu.kevinjiang.netsec.core.AsymmetricCryptoHelper;
import edu.neu.kevinjiang.netsec.core.DiffieHellmanUtil;
import edu.neu.kevinjiang.netsec.core.DigestUtil;
import edu.neu.kevinjiang.netsec.core.JavaSerializer;
import edu.neu.kevinjiang.netsec.core.protocol.MessageEnvelope;
import edu.neu.kevinjiang.netsec.core.protocol.peer.PeerAuthenticate;
import edu.neu.kevinjiang.netsec.core.protocol.peer.PeerHello;
import edu.neu.kevinjiang.netsec.core.protocol.peer.PeerHelloResponse;
import org.bouncycastle.jce.provider.JCEDHPublicKey;

import javax.crypto.SecretKey;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Map;

/**
 * Created by kevinbjiang on 7/26/14.
 */
public class PeerClientConnectionListener extends Listener
{

    private static final SecureRandom random = new SecureRandom();

    final private PeerClient client;
    final private PeerClientConnectionState state;

    public PeerClientConnectionListener(PeerClientConnectionState state, PeerClient client)
    {
        this.state = state;
        this.client = client;
    }

    @Override
    public void connected(Connection connection)
    {
        super.connected(connection);
    }

    @Override
    public void disconnected(Connection connection)
    {
        super.disconnected(connection);

        //remove from client registry
        Map<String, PeerClient> clientRegistry = App.getInstance().getPeerClientManager().getClientRegistry();
        clientRegistry.remove(state.getPeerUser());

//        String peerUser = state.getPeerUser();
//        if (peerUser != null)
//        {
//            System.out.println("User [" + peerUser + "] disconnected");
//        }
//        else
//        {
//            System.out.println("Unauthenticated client disconnected");
//        }

        //update session state
        state.setState(PeerClientState.DISCONNECTED);
    }

    @Override
    public void received(Connection connection, Object object)
    {
        if (object instanceof FrameworkMessage)
        {
            return;
        }

        if (!(object instanceof MessageEnvelope))
        {
            System.out.println("Invalid message format received");
            return;
        }

        Object message;
        MessageEnvelope envelope = (MessageEnvelope)object;
        if (envelope.encrypted)
        {

            SecretKey aesKey = state.getAesKey();
            if (aesKey == null)
            {
                System.out.println("Sender encrypted message before session key established. Dropping message");
                return;
            }

            message = envelope.read(aesKey);
        }
        else
        {
            message = envelope.read(null);
        }

        //System.out.println("Received message: " + message.getClass().getName());

        if (message instanceof PeerHelloResponse)
        {
            handlePeerResponse((PeerHelloResponse) message);
        }
        else
        {
            System.out.println("Dropping unexpected protocol message received from peer: " + message.getClass().getName());
        }
    }

    public void handlePeerResponse(PeerHelloResponse response)
    {
        synchronized (state)
        {
            if (state.getState() != PeerClientState.SENT_DH_PUBLIC)
            {
                return;
            }

            //decrypt message
            response.decrypt(state.getSelfPrivateKey());

            //check challenge response
            byte[] receivedChallengeResponse = response.getInitiatorChallengeNonceResponse();
            if (!Arrays.equals(receivedChallengeResponse, state.getSentChallengeNonce()))
            {
                System.out.println("Challenge failed");
                return;
            }

            //derive diffie hellman key and aes session key
            DiffieHellmanUtil diffieHellmanUtil = state.getDiffieHellmanUtil();
            JCEDHPublicKey peerDHPublicKey = diffieHellmanUtil.deserializeDHPublicKey(response.getResponderDHPublicKey());
            SecretKey aesKey = diffieHellmanUtil.deriveAESKeyFromPublicKeys(peerDHPublicKey);

            state.setAesKey(aesKey);

            //build peerAuthenticate
            PeerAuthenticate peerAuthenticate = new PeerAuthenticate(response.getResponderChallengeNonce(), state.getInitialMessage());

            //send peerAuthenticate
            client.send(peerAuthenticate, state.getAesKey());

            //transition
            state.setState(PeerClientState.AUTHENTICATED);
        }
    }


}
