package edu.neu.kevinjiang.netsec.client.peer.client;

import edu.neu.kevinjiang.netsec.core.DiffieHellmanUtil;

import javax.crypto.SecretKey;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * Created by kevinbjiang on 8/2/14.
 */
public class PeerClientConnectionState
{
    //changes throughout the session
    private PeerClientState state = PeerClientState.DISCONNECTED;
    private long lastSentMessageId = 0;

    //set on initialization
    private PrivateKey selfPrivateKey;
    private String peerUser;
    private String address;
    private int port;
    private PublicKey peerPublicKey;
    private String initialMessage;

    //set as the handshake proceeds
    final private DiffieHellmanUtil diffieHellmanUtil = new DiffieHellmanUtil();
    private byte[] sentChallengeNonce;
    private SecretKey aesKey;

    public DiffieHellmanUtil getDiffieHellmanUtil()
    {
        return diffieHellmanUtil;
    }

    public PeerClientState getState()
    {
        return state;
    }

    public void setState(PeerClientState state)
    {
        this.state = state;
    }

    public long getLastSentMessageId()
    {
        return lastSentMessageId;
    }

    public void setLastSentMessageId(long lastSentMessageId)
    {
        this.lastSentMessageId = lastSentMessageId;
    }

    public PrivateKey getSelfPrivateKey()
    {
        return selfPrivateKey;
    }

    public void setSelfPrivateKey(PrivateKey selfPrivateKey)
    {
        this.selfPrivateKey = selfPrivateKey;
    }

    public String getPeerUser()
    {
        return peerUser;
    }

    public void setPeerUser(String peerUser)
    {
        this.peerUser = peerUser;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public PublicKey getPeerPublicKey()
    {
        return peerPublicKey;
    }

    public void setPeerPublicKey(PublicKey peerPublicKey)
    {
        this.peerPublicKey = peerPublicKey;
    }

    public byte[] getSentChallengeNonce()
    {
        return sentChallengeNonce;
    }

    public void setSentChallengeNonce(byte[] sentChallengeNonce)
    {
        this.sentChallengeNonce = sentChallengeNonce;
    }

    public SecretKey getAesKey()
    {
        return aesKey;
    }

    public void setAesKey(SecretKey aesKey)
    {
        this.aesKey = aesKey;
    }

    public String getInitialMessage()
    {
        return initialMessage;
    }

    public void setInitialMessage(String initialMessage)
    {
        this.initialMessage = initialMessage;
    }
}
