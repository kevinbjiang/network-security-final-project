package edu.neu.kevinjiang.netsec.server;

import java.io.IOException;
import java.security.Security;

/**
 * Created by kevinbjiang on 7/26/14.
 */
public class ChatServerMain
{
    public static void main(String[] args) throws IOException
    {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

        ChatServer chatServer = ChatServer.getInstance();
        chatServer.bind(ServerConfig.getInstance().getServerPort());
    }
}
