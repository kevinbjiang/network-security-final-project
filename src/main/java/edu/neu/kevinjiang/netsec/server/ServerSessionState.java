package edu.neu.kevinjiang.netsec.server;

import com.esotericsoftware.kryonet.Connection;
import edu.neu.kevinjiang.netsec.core.*;
import edu.neu.kevinjiang.netsec.core.protocol.login.LoginAuthenticate;
import edu.neu.kevinjiang.netsec.core.protocol.login.LoginAuthenticateResponse;
import edu.neu.kevinjiang.netsec.core.protocol.login.LoginHello;
import edu.neu.kevinjiang.netsec.core.protocol.login.LoginHelloResponse;
import edu.neu.kevinjiang.netsec.core.protocol.session.*;
import org.bouncycastle.jce.provider.JCEDHPublicKey;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.Set;

/**
 * Created by kevinbjiang on 8/1/14.
 */
public class ServerSessionState
{
    //app
    private PrivateKey privateKey = ChatServer.getInstance().getPrivateKey();

    //utils
    private DiffieHellmanUtil diffieHellmanUtil = new DiffieHellmanUtil();
    private SymmetricCryptoHelper symmetricCryptoHelper = new SymmetricCryptoHelper();
    private AsymmetricCryptoHelper asymmetricCryptoHelper = new AsymmetricCryptoHelper();
    private DigestUtil digestUtil = new DigestUtil();
    private JavaSerializer javaSerializer = new JavaSerializer();

    private ChatServer server = ChatServer.getInstance();
    private UserRegistry userRegistry = UserRegistry.getInstance();
    private ConnectionRegistry connectionRegistry = ConnectionRegistry.getInstance();

    //session data
    protected String user;
    protected String address;
    protected int port;
    protected int connectionId;
    protected PublicKey publicKey;
    protected SecretKey aesKey;
    protected long lastMessageId = 0;

    //state
    protected SessionState state = SessionState.DISCONNECTED;

    public ServerSessionState(int connectionId)
    {
        this.connectionId = connectionId;
    }

    public void handleLoginHello(LoginHello request)
    {
        synchronized (this)
        {
            if (state != SessionState.DISCONNECTED)
            {
                return;
            }

            PublicKey dhClientPublicKey;
            try
            {
                dhClientPublicKey = (PublicKey)javaSerializer.deserialize(request.dhClientPublicKey);
            }
            catch (ClassCastException e)
            {
                System.out.println("Unexpected data received instead of dh public key");
                return;
            }

            byte[] challengeNonce = request.challengeNonce;

            //derive dh session key
            JCEDHPublicKey jcedhPublicKey;
            try
            {
                jcedhPublicKey = (JCEDHPublicKey)dhClientPublicKey;
            }
            catch (ClassCastException e)
            {
                System.out.println("Unexpected dh public key type received" + e);
                return;
            }

            PublicKey dhPublicKey = diffieHellmanUtil.init(jcedhPublicKey);
            byte[] dhSessionKey = diffieHellmanUtil.deriveDHKey(dhClientPublicKey);

            //derive aes key from dh session key
            byte[] sha1DiffieKey = digestUtil.sha1(dhSessionKey);
            byte[] aesKeyBytes = Arrays.copyOfRange(sha1DiffieKey, 0, 16);
            aesKey = new SecretKeySpec(aesKeyBytes, 0, aesKeyBytes.length, "AES");

            //sign challenge nonce
            final byte[] challengeResponse = asymmetricCryptoHelper.sign(privateKey, challengeNonce);

            //build response
            LoginHelloResponse response = new LoginHelloResponse();
            response.challengeNonceResponse = challengeResponse;
            response.dhServerPublicKey = javaSerializer.serialize(dhPublicKey);

            //send response
            server.send(connectionId, response, false, null);

            //transition
            state = SessionState.SECURE_CHANNEL_ESTABLISHED;
        }
    }

    public void handleLoginAuthenticate(LoginAuthenticate request)
    {
        synchronized (this)
        {
            if (state != SessionState.SECURE_CHANNEL_ESTABLISHED)
            {
                return;
            }

            String username = request.username;
            String password = request.password;
            int port = request.port;

            LoginAuthenticateResponse loginAuthenticateResponse = new LoginAuthenticateResponse();

            //only allow one session per user
            boolean userSessionExists = connectionRegistry.getUserList().contains(username);
            boolean authenticated = userRegistry.authenticate(username, password);
            if (authenticated && !userSessionExists)
            {
                //store user
                this.user = username;

                //store port
                this.port = port;

                //generate key pair
                KeyPair keyPair = asymmetricCryptoHelper.generateKeyPair();
                publicKey = keyPair.getPublic();
                PrivateKey privateKey = keyPair.getPrivate();

                //prepare response
                loginAuthenticateResponse.authenticated = true;
                loginAuthenticateResponse.sessionPrivateKey = privateKey;

                //transition
                state = SessionState.AUTHENTICATED;
            }
            else
            {
                loginAuthenticateResponse.authenticated = false;
            }

            //send response
            server.send(connectionId, loginAuthenticateResponse, true, aesKey);
        }
    }

    public void handleHeartbeatRequest(HeartbeatRequest request)
    {
        synchronized (this)
        {
            if (state != SessionState.AUTHENTICATED)
            {
                System.out.println("Cannot handle session request when not yet authenticated");
                return;
            }

            if (lastMessageId >= request.messageId)
            {
                System.out.println("Received expired messageId");
                return;
            }

            //update latest message id
            lastMessageId = request.messageId;

            //prepare response
            HeartbeatResponse heartbeatResponse = new HeartbeatResponse();
            heartbeatResponse.inResponseTo = request.messageId;

            //send response
            server.send(connectionId, heartbeatResponse, true, aesKey);
        }
    }

    public void handleListRequest(ListRequest request)
    {
        synchronized (this)
        {
            if (state != SessionState.AUTHENTICATED)
            {
                System.out.println("Cannot handle session request when not yet authenticated");
                return;
            }

            if (lastMessageId >= request.messageId)
            {
                System.out.println("Received expired messageId");
                return;
            }

            //update latest message id
            lastMessageId = request.messageId;

            //get list of all users
            Set<String> userList = connectionRegistry.getUserList();

            //prepare response
            ListResponse listResponse = new ListResponse();
            listResponse.inResponseTo = lastMessageId;
            listResponse.users = userList;

            //send response
            server.send(connectionId, listResponse, true, aesKey);
        }
    }

    public void handleClientInfoRequest(ClientInfoRequest request)
    {
        synchronized (this)
        {
            if (state != SessionState.AUTHENTICATED)
            {
                System.out.println("Cannot handle session request when not yet authenticated");
                return;
            }

            if (lastMessageId >= request.messageId)
            {
                System.out.println("Received expired messageId");
                return;
            }

            //update latest message id
            lastMessageId = request.messageId;

            //retrieve user info
            String username = request.username;

            //create response
            ClientInfoResponse clientInfoResponse = new ClientInfoResponse();
            clientInfoResponse.inResponseTo = request.messageId;

            //look through sessions for user
            ServerSessionState sessionForUser = connectionRegistry.getSessionForUser(username);
            if (sessionForUser != null)
            {
                String address = sessionForUser.getAddress();
                int port = sessionForUser.getPort();
                PublicKey publicKey = sessionForUser.getPublicKey();

                clientInfoResponse.success = true;
                clientInfoResponse.username = username;
                clientInfoResponse.address = address;
                clientInfoResponse.port = port;
                clientInfoResponse.clientPublicKey = publicKey;
            }
            else
            {
                clientInfoResponse.username = username;
                clientInfoResponse.success = false;
            }

            //send response
            server.send(connectionId, clientInfoResponse, true, aesKey);
        }
    }

    public int getConnectionId()
    {
        return connectionId;
    }

    public SecretKey getAesKey()
    {
        return aesKey;
    }

    public String getUser()
    {
        return user;
    }

    public PublicKey getPublicKey()
    {
        return publicKey;
    }

    public String getAddress()
    {
        return address;
    }

    public int getPort()
    {
        return port;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }
}
