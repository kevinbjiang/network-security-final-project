package edu.neu.kevinjiang.netsec.server;

import edu.neu.kevinjiang.netsec.core.PBKDFCryptoHelper;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by kevinbjiang on 8/1/14.
 */
public class UserRegistry
{
    private final static UserRegistry _registry = new UserRegistry();
    public final static UserRegistry getInstance()
    {
        return _registry;
    }

    private Map<String, UserData> registry = new ConcurrentHashMap<>();
    private Random random = new SecureRandom();
    private PBKDFCryptoHelper pbkdfCryptoHelper = new PBKDFCryptoHelper();


    private UserRegistry()
    {
        createTestAccounts();
    }

    public boolean authenticate(String username, String password)
    {
        //lookup user
        UserData userData = registry.get(username.toLowerCase());
        if (userData != null)
        {
            byte[] salt = userData.getSalt();
            byte[] calculatedPasswordHash = pbkdfCryptoHelper.deriveKeyFromPassword(password, salt);

            return Arrays.equals(calculatedPasswordHash, userData.getHash());
        }

        return false;
    }

    public boolean register(String username, String password)
    {
        if (!registry.containsKey(username))
        {
            //generate random salt
            byte[] salt = new byte[16];
            random.nextBytes(salt);

            //create new user data
            byte[] calculatedHash = pbkdfCryptoHelper.deriveKeyFromPassword(password, salt);

            UserData userData = new UserData(username, salt, calculatedHash);
            registry.put(username, userData);

            return true;
        }

        return false;
    }

    /**
     * for demo only, non-production
     */
    private void createTestAccounts()
    {
        register("guest", "password");
        register("kevin", "northeastern");
        register("sam", "iphone");
        register("bob", "mustang");
        register("admin", "mko0nji9");
    }
}
