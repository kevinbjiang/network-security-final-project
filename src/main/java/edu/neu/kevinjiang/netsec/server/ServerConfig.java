package edu.neu.kevinjiang.netsec.server;

import edu.neu.kevinjiang.netsec.core.PropertiesReader;

/**
 * Created by kevinbjiang on 8/1/14.
 */
public class ServerConfig
{
    static final private ServerConfig _instance = new ServerConfig();
    static final public ServerConfig getInstance()
    {
        return _instance;
    }


    private PropertiesReader propertiesReader;

    public ServerConfig()
    {
        //read from properties
        propertiesReader = new PropertiesReader("server.properties");

        serverPrivateKeyPath = (String) propertiesReader.getProperties().getProperty("serverPrivateKeyPath", "server_private.pem");
        serverPort = Integer.valueOf(propertiesReader.getProperties().getProperty("serverPort", "55443"));
    }

    private String serverPrivateKeyPath;
    private int serverPort;

    public String getServerPrivateKeyPath()
    {
        return serverPrivateKeyPath;
    }

    public int getServerPort()
    {
        return serverPort;
    }
}
