package edu.neu.kevinjiang.netsec.server;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by kevinbjiang on 8/1/14.
 */
public class ConnectionRegistry
{
    private static final ConnectionRegistry _registry = new ConnectionRegistry();
    public static ConnectionRegistry getInstance()
    {
        return _registry;
    }

    private ConnectionRegistry()
    {

    }

    protected Map<Integer, ServerSessionState> registry = new ConcurrentHashMap<>();

    public ServerSessionState get(Integer connectionId)
    {
        synchronized (this)
        {
            return registry.get(connectionId);
        }
    }

    public boolean add(ServerSessionState state)
    {
        synchronized (this)
        {
            if (registry.containsKey(state.getConnectionId()))
            {
                return false;
            }

            registry.put(state.getConnectionId(), state);
            return true;
        }
    }

    public Set<String> getUserList()
    {
        Set<String> userList = new HashSet<>();
        synchronized (this)
        {
            for (Map.Entry<Integer, ServerSessionState> entry : registry.entrySet())
            {
                String user = entry.getValue().getUser();
                userList.add(user);
            }

            return userList;
        }
    }

    public ServerSessionState getSessionForUser(String user)
    {
        synchronized (this)
        {
            for (Map.Entry<Integer, ServerSessionState> entry : registry.entrySet())
            {
                if (user.equals(entry.getValue().getUser()))
                {
                    return entry.getValue();
                }
            }

            return null;
        }
    }

    public void removeConnection(Integer connectionId)
    {
        synchronized (this)
        {
            registry.remove(connectionId);
        }
    }


}
