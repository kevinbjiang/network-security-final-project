package edu.neu.kevinjiang.netsec.server;

import java.security.PrivateKey;

/**
 * Created by kevinbjiang on 8/1/14.
 */
public class UserData
{
    private String username;
    private byte[] salt;
    private byte[] hash;

    public UserData(String username, byte[] salt, byte[] hash)
    {
        this.username = username;
        this.salt = salt;
        this.hash = hash;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public byte[] getSalt()
    {
        return salt;
    }

    public void setSalt(byte[] salt)
    {
        this.salt = salt;
    }

    public byte[] getHash()
    {
        return hash;
    }

    public void setHash(byte[] hash)
    {
        this.hash = hash;
    }
}

