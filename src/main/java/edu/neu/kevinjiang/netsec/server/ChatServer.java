package edu.neu.kevinjiang.netsec.server;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Server;
import com.esotericsoftware.minlog.Log;

import edu.neu.kevinjiang.netsec.core.PEMKeyPairReader;
import edu.neu.kevinjiang.netsec.core.SymmetricCryptoEncryptResult;
import edu.neu.kevinjiang.netsec.core.protocol.MessageEnvelope;
import edu.neu.kevinjiang.netsec.core.protocol.login.LoginHello;
import edu.neu.kevinjiang.netsec.core.protocol.login.LoginHelloResponse;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import java.io.IOException;
import java.io.Serializable;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidParameterSpecException;

/**
 * Created by kevinbjiang on 8/1/14.
 */
public class ChatServer
{
    private static final ChatServer _server = new ChatServer();
    public static ChatServer getInstance()
    {
        return _server;
    }

    private Server server;
    private PrivateKey privateKey;

    private ChatServer()
    {
        Log.set(Log.LEVEL_NONE);

        PEMKeyPairReader pemKeyPairReader = new PEMKeyPairReader();
        try
        {
            privateKey = pemKeyPairReader.readPrivateKey(ServerConfig.getInstance().getServerPrivateKeyPath());
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

        this.server = new Server();
        Kryo kryo = this.server.getKryo();
        kryo.register(MessageEnvelope.class);
        kryo.register(LoginHello.class);
        kryo.register(LoginHelloResponse.class);
        kryo.register(byte[].class);
        kryo.register(org.bouncycastle.jce.provider.JCEDHPublicKey.class);
        kryo.register(javax.crypto.spec.DHParameterSpec.class);
        kryo.register(java.math.BigInteger.class);
        kryo.register(SymmetricCryptoEncryptResult.class);
    }

    public void bind(int port) throws IOException
    {
        System.out.println("Listening on port " + port);

        server.start();
        server.addListener(new ServerConnectionListener());
        server.bind(port);
    }

    public void send(int connectionId, Serializable message, boolean encrypt, SecretKey key)
    {
        //System.out.println("Sending message: " + message.getClass().getName());

        try
        {
            MessageEnvelope envelope = new MessageEnvelope(message, key);
            server.sendToTCP(connectionId, envelope);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (NoSuchPaddingException e)
        {
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        catch (IllegalBlockSizeException e)
        {
            e.printStackTrace();
        }
        catch (BadPaddingException e)
        {
            e.printStackTrace();
        }
        catch (InvalidKeyException e)
        {
            e.printStackTrace();
        }
        catch (InvalidParameterSpecException e)
        {
            e.printStackTrace();
        }
    }

    public void close()
    {
        System.out.println("Closing server connection");
        server.close();
    }

    public PrivateKey getPrivateKey()
    {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey)
    {
        this.privateKey = privateKey;
    }
}
