package edu.neu.kevinjiang.netsec.server;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage;
import com.esotericsoftware.kryonet.Listener;
import edu.neu.kevinjiang.netsec.core.protocol.MessageEnvelope;
import edu.neu.kevinjiang.netsec.core.protocol.login.LoginAuthenticate;
import edu.neu.kevinjiang.netsec.core.protocol.login.LoginAuthenticateResponse;
import edu.neu.kevinjiang.netsec.core.protocol.login.LoginHello;
import edu.neu.kevinjiang.netsec.core.protocol.session.ClientInfoRequest;
import edu.neu.kevinjiang.netsec.core.protocol.session.HeartbeatRequest;
import edu.neu.kevinjiang.netsec.core.protocol.session.ListRequest;

import javax.crypto.SecretKey;

/**
 * Created by kevinbjiang on 7/26/14.
 */
public class ServerConnectionListener extends Listener
{
    protected ConnectionRegistry connectionRegistry = ConnectionRegistry.getInstance();

    @Override
    public void connected(Connection connection)
    {
        super.connected(connection);
    }

    @Override
    public void disconnected(Connection connection)
    {
        super.disconnected(connection);

        //if user disconnects, remove them from the connection registry
        connectionRegistry.removeConnection(connection.getID());
    }

    @Override
    public void received(Connection connection, Object object)
    {
        super.received(connection, object);

        if (object instanceof FrameworkMessage)
        {
            return;
        }

        if (!(object instanceof MessageEnvelope))
        {
            System.out.println("Invalid message format received: " + object.getClass().getName());
            return;
        }

        //check if we have a session, if not create one
        ServerSessionState serverSessionState = connectionRegistry.get(connection.getID());
        if (serverSessionState == null)
        {
            serverSessionState = new ServerSessionState(connection.getID());
            connectionRegistry.add(serverSessionState);
        }

        Object message = null;
        MessageEnvelope envelope = (MessageEnvelope)object;

        if (envelope.encrypted)
        {
            SecretKey aesKey = serverSessionState.getAesKey();
            if (aesKey == null)
            {
                System.out.println("Sender encrypted message before session key established. Dropping message");
                return;
            }

            message = envelope.read(aesKey);
        }
        else
        {
            message = envelope.read(null);
        }

        //System.out.println("Received message: " + message.getClass().getName());
        serverSessionState.setAddress(connection.getRemoteAddressTCP().getHostString());

        if (message instanceof LoginAuthenticate)
        {
            serverSessionState.handleLoginAuthenticate((LoginAuthenticate) message);
        }
        else if (message instanceof LoginHello)
        {
            serverSessionState.handleLoginHello((LoginHello) message);
        }
        else if (message instanceof ClientInfoRequest)
        {
            serverSessionState.handleClientInfoRequest((ClientInfoRequest) message);
        }
        else if (message instanceof HeartbeatRequest)
        {
            serverSessionState.handleHeartbeatRequest((HeartbeatRequest) message);
        }
        else if (message instanceof ListRequest)
        {
            serverSessionState.handleListRequest((ListRequest) message);
        }
        else
        {
            System.out.println("Dropping unexpected protocol message received from client: " + message.getClass().getName());
        }
    }
}
