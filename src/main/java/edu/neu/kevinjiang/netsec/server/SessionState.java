package edu.neu.kevinjiang.netsec.server;

/**
 * Created by kevinbjiang on 8/1/14.
 */
public enum SessionState
{
    DISCONNECTED,
    SECURE_CHANNEL_ESTABLISHED,
    AUTHENTICATED
}
