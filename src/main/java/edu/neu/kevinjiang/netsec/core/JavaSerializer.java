package edu.neu.kevinjiang.netsec.core;

import java.io.*;

/**
 * Created by kevinbjiang on 8/1/14.
 */
public class JavaSerializer
{
    public byte[] serialize(Serializable object)
    {
        byte[] objectBytes = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try (ObjectOutputStream oos = new ObjectOutputStream(bos))
        {
            oos.writeObject(object);
            oos.flush();
            bos.flush();

            byte[] data = bos.toByteArray();
            return data;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public Object deserialize(byte[] data)
    {
        ByteArrayInputStream bis = new ByteArrayInputStream(data);
        try (ObjectInputStream ois = new ObjectInputStream(bis);)
        {
            Object o = ois.readObject();
            return o;
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
