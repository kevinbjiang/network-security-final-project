package edu.neu.kevinjiang.netsec.core;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;

/**
 * Created by kevinbjiang on 7/29/14.
 */
public class AsymmetricCryptoHelper
{
    final static String PUBLIC_KEY_CIPHER_ALGORITHM = "RSA";
    final static String PUBLIC_KEY_SIGNATURE_ALGORITHM = "SHA256withRSA";
    final static String PUBLIC_KEY_SIGNATURE_HASH_ALGORITHM = "SHA-256";

    public boolean validateSignature(PublicKey publicKey, byte[] digestBytes, byte[] signature)
    {
        try
        {
            Signature validatingSignature = Signature.getInstance(PUBLIC_KEY_SIGNATURE_ALGORITHM);
            validatingSignature.initVerify(publicKey);
            validatingSignature.update(digestBytes);
            boolean verified = validatingSignature.verify(signature);

            return verified;
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        catch (InvalidKeyException e)
        {
            e.printStackTrace();
        }
        catch (SignatureException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public byte[] sign(PrivateKey privateKey, byte[] digestBytes)
    {
        try
        {
            Signature signingSignature = Signature.getInstance(PUBLIC_KEY_SIGNATURE_ALGORITHM);
            signingSignature.initSign(privateKey);
            signingSignature.update(digestBytes);
            byte[] signatureBytes = signingSignature.sign();

            return signatureBytes;
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        catch (InvalidKeyException e)
        {
            e.printStackTrace();
        }
        catch (SignatureException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public byte[] encrypt(PublicKey publicKey, byte[] data)
    {
        try
        {
            Cipher encryptCipher = Cipher.getInstance(PUBLIC_KEY_CIPHER_ALGORITHM);
            encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);
            encryptCipher.update(data);
            byte[] encryptedData = encryptCipher.doFinal();

            return encryptedData;
        }
        catch (InvalidKeyException e)
        {
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        catch (NoSuchPaddingException e)
        {
            e.printStackTrace();
        }
        catch (BadPaddingException e)
        {
            e.printStackTrace();
        }
        catch (IllegalBlockSizeException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public byte[] decrypt(PrivateKey privateKey, byte[] data)
    {
        try
        {
            Cipher decryptCipher = Cipher.getInstance(PUBLIC_KEY_CIPHER_ALGORITHM);
            decryptCipher.init(Cipher.DECRYPT_MODE, privateKey);
            decryptCipher.update(data);
            byte[] decryptedData = decryptCipher.doFinal();

            return decryptedData;
        }
        catch (IllegalBlockSizeException e)
        {
            e.printStackTrace();
        }
        catch (InvalidKeyException e)
        {
            e.printStackTrace();
        }
        catch (BadPaddingException e)
        {
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        catch (NoSuchPaddingException e)
        {
            e.printStackTrace();
        }

        return null;
    }


    public KeyPair generateKeyPair()
    {
        KeyPairGenerator keyGen = null;
        try
        {
            keyGen = KeyPairGenerator.getInstance(PUBLIC_KEY_CIPHER_ALGORITHM);
            keyGen.initialize(1024);
            return keyGen.generateKeyPair();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }

        return null;
    }

}
