package edu.neu.kevinjiang.netsec.core.protocol.session;

import java.io.Serializable;
import java.security.PublicKey;


/**
 * Created by kevinbjiang on 7/26/14.
 */
public class ClientInfoResponse implements Serializable
{
    public long inResponseTo;
    public String username;
    public boolean success;
    public String address;
    public int port;
    public PublicKey clientPublicKey;

    @Override
    public String toString()
    {
        return "ClientInfoResponse{" +
                "inResponseTo=" + inResponseTo +
                ", username='" + username + '\'' +
                ", success=" + success +
                ", address='" + address + '\'' +
                ", port=" + port +
                ", clientPublicKey=" + clientPublicKey +
                '}';
    }
}
