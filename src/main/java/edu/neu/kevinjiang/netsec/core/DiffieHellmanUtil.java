package edu.neu.kevinjiang.netsec.core;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.provider.JCEDHPublicKey;

import javax.crypto.KeyAgreement;
import javax.crypto.SecretKey;
import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DHParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigInteger;
import java.security.*;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;

/**
 * Created by kevinbjiang on 7/27/14.
 */
public class DiffieHellmanUtil
{
    private final static SecureRandom secureRandom = new SecureRandom();
    private final static JavaSerializer javaSerializer = new JavaSerializer();
    private final static DigestUtil digestUtil = new DigestUtil();

    KeyPairGenerator keyPairGenerator;
    KeyPair keyPair;

    public DiffieHellmanUtil()
    {
    }

    /**
     *
     * Generates a diffie hellman key pair
     *
     * @return
     */
    public PublicKey init()
    {
        try
        {
            Security.addProvider(new BouncyCastleProvider());
            keyPairGenerator = KeyPairGenerator.getInstance("DH", "BC");
            keyPairGenerator.initialize(512, secureRandom);

            keyPair = keyPairGenerator.generateKeyPair();
            return keyPair.getPublic();
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new RuntimeException(e);
        }
        catch (NoSuchProviderException e)
        {
            throw new RuntimeException(e);
        }
    }

    /**
     * Generates a difife hellman key pair using another public key's params as the p/g
     *
     * @param otherPublicKey
     * @return
     */
    public PublicKey init(JCEDHPublicKey otherPublicKey)
    {
        try
        {
            Security.addProvider(new BouncyCastleProvider());
            keyPairGenerator = KeyPairGenerator.getInstance("DH", "BC");
            keyPairGenerator.initialize(otherPublicKey.getParams(), secureRandom);

            keyPair = keyPairGenerator.generateKeyPair();
            return keyPair.getPublic();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        catch (NoSuchProviderException e)
        {
            e.printStackTrace();
        }
        catch (InvalidAlgorithmParameterException e)
        {
            e.printStackTrace();
        }
        return null;


    }

    /**
     *
     * Combines local diffie hellman private key with other party's public key to derive the diffie hellman session key
     *
     * @param otherKey
     * @return
     * @throws NoSuchProviderException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    public byte[] deriveDHKey(PublicKey otherKey)
    {
        KeyAgreement keyAgreement = null;
        try
        {
            keyAgreement = KeyAgreement.getInstance("DH", "BC");
            keyAgreement.init(keyPair.getPrivate());
            keyAgreement.doPhase(otherKey, true);
        }
        catch (InvalidKeyException e)
        {
            throw new RuntimeException(e);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new RuntimeException(e);
        }
        catch (NoSuchProviderException e)
        {
            throw new RuntimeException(e);
        }

        byte[] dhKey = keyAgreement.generateSecret();
        return dhKey;
    }

    public SecretKey deriveAESKeyFromPublicKeys(PublicKey peerPublicKey)
    {
        //user peer public dh key to derive secret dh key
        byte[] dhKey = deriveDHKey(peerPublicKey);

        //derive aes session key
        byte[] sha1DiffieKey = digestUtil.sha1(dhKey);
        byte[] aesKeyBytes = Arrays.copyOfRange(sha1DiffieKey, 0, 16);
        SecretKey aesKey = new SecretKeySpec(aesKeyBytes, 0, aesKeyBytes.length, "AES");

        return aesKey;
    }


    public byte[] serializeDhPublicKey(PublicKey key)
    {
        return javaSerializer.serialize(key);
    }

    public JCEDHPublicKey deserializeDHPublicKey(byte[] bytes)
    {
        PublicKey dhPeerPublicKey;
        try
        {
            dhPeerPublicKey = (PublicKey)javaSerializer.deserialize(bytes);
        }
        catch (ClassCastException e)
        {
            System.out.println("Unexpected data received instead of dh public key");
            return null;
        }

        JCEDHPublicKey jcedhPublicKey;
        try
        {
            jcedhPublicKey = (JCEDHPublicKey)dhPeerPublicKey;
        }
        catch (ClassCastException e)
        {
            System.out.println("Unexpected dh public key type received" + e);
            return null;
        }

        return jcedhPublicKey;
    }




    public void test() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException
    {
        KeyPair keyPairA = keyPairGenerator.generateKeyPair();
        KeyPair keyPairB = keyPairGenerator.generateKeyPair();

        KeyAgreement keyAgreementA = KeyAgreement.getInstance("DH", "BC");
        keyAgreementA.init(keyPairA.getPrivate());
        keyAgreementA.doPhase(keyPairB.getPublic(), true);
        byte[] finalSecretA = keyAgreementA.generateSecret();

        KeyAgreement keyAgreementB = KeyAgreement.getInstance("DH", "BC");
        keyAgreementB.init(keyPairB.getPrivate());
        keyAgreementB.doPhase(keyPairA.getPublic(), true);
        byte[] finalSecretB = keyAgreementB.generateSecret();

        System.out.println("final secret A");
        System.out.println(javax.xml.bind.DatatypeConverter.printBase64Binary(finalSecretA));

        System.out.println("final secret B");
        System.out.println(javax.xml.bind.DatatypeConverter.printBase64Binary(finalSecretB));
    }
}
