package edu.neu.kevinjiang.netsec.core.protocol.login;

import java.io.Serializable;
import java.security.PublicKey;

/**
 * Created by kevinbjiang on 7/26/14.
 */
public class LoginHelloResponse implements Serializable
{
    public byte[] challengeNonceResponse;
    public byte[] dhServerPublicKey;
}
