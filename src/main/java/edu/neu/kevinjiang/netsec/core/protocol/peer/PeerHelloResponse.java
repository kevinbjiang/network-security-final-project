package edu.neu.kevinjiang.netsec.core.protocol.peer;

import edu.neu.kevinjiang.netsec.core.AsymmetricCryptoHelper;
import edu.neu.kevinjiang.netsec.core.JavaSerializer;
import edu.neu.kevinjiang.netsec.core.SymmetricCryptoEncryptResult;
import edu.neu.kevinjiang.netsec.core.SymmetricCryptoHelper;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.*;
import java.security.spec.InvalidParameterSpecException;
import java.util.Arrays;

/**
 * Created by kevinbjiang on 7/26/14.
 */
public class PeerHelloResponse implements Serializable
{
    final private static AsymmetricCryptoHelper asymmetricCryptoHelper = new AsymmetricCryptoHelper();
    final private static SymmetricCryptoHelper symmetricCryptoHelper = new SymmetricCryptoHelper();

    //public key encrypted
    private SymmetricCryptoEncryptResult encryptedData;
    private byte[] encryptedAesKey;

    //transient
    private transient byte[] responderDHPublicKey;
    private transient byte[] initiatorChallengeNonceResponse;
    private transient byte[] responderChallengeNonce;

    public PeerHelloResponse()
    {
    }

    public PeerHelloResponse(byte[] responderChallengeNonce, byte[] initiatorChallengeNonceResponse, byte[] responderDHPublicKey)
    {
        this.responderChallengeNonce = responderChallengeNonce;
        this.initiatorChallengeNonceResponse = initiatorChallengeNonceResponse;
        this.responderDHPublicKey = responderDHPublicKey;
    }

    public byte[] getResponderChallengeNonce()
    {
        return responderChallengeNonce;
    }

    public byte[] getInitiatorChallengeNonceResponse()
    {
        return initiatorChallengeNonceResponse;
    }

    public byte[] getResponderDHPublicKey()
    {
        return responderDHPublicKey;
    }

    public void decrypt(PrivateKey key)
    {
        try
        {
            if (encryptedData != null && encryptedAesKey != null)
            {
                //decrypt aes key
                byte[] decryptedAesBytes = asymmetricCryptoHelper.decrypt(key, encryptedAesKey);
                SecretKey aesKey = new SecretKeySpec(decryptedAesBytes, "AES");

                //decrypt data
                byte[] decryptedBytes = symmetricCryptoHelper.decrypt(encryptedData.getCipherText(), encryptedData.getIv(), aesKey);

                //deserialize data
                ByteArrayInputStream bis = new ByteArrayInputStream(decryptedBytes);
                try (ObjectInputStream ois = new ObjectInputStream(bis))
                {
                    responderDHPublicKey = (byte[]) ois.readObject();
                    initiatorChallengeNonceResponse = (byte[]) ois.readObject();
                    responderChallengeNonce = (byte[]) ois.readObject();
                }
                catch (ClassNotFoundException e)
                {
                    e.printStackTrace();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
        catch (NoSuchPaddingException e)
        {
            e.printStackTrace();
        }
        catch (InvalidKeyException e)
        {
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        catch (IllegalBlockSizeException e)
        {
            e.printStackTrace();
        }
        catch (BadPaddingException e)
        {
            e.printStackTrace();
        }
        catch (InvalidAlgorithmParameterException e)
        {
            e.printStackTrace();
        }

    }

    public void encrypt(PublicKey key)
    {
        try
        {
            if (responderDHPublicKey != null && initiatorChallengeNonceResponse != null && responderChallengeNonce != null)
            {
                //generate aes key
                SecretKey aesKey = symmetricCryptoHelper.generateRandomKey();

                //encrypt aes key
                byte[] aesKeyBytes = aesKey.getEncoded();
                byte[] encryptedAesKeyBytes = asymmetricCryptoHelper.encrypt(key, aesKeyBytes);
                this.encryptedAesKey = encryptedAesKeyBytes;

                //encrypt data with aes key
                byte[] objectsBytes = null;
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                try (ObjectOutputStream oos = new ObjectOutputStream(bos))
                {
                    oos.writeObject(responderDHPublicKey);
                    oos.writeObject(initiatorChallengeNonceResponse);
                    oos.writeObject(responderChallengeNonce);
                    oos.flush();
                    bos.flush();

                    objectsBytes = bos.toByteArray();
                    encryptedData = symmetricCryptoHelper.encrypt(objectsBytes, aesKey);
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
        catch (NoSuchPaddingException e)
        {
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        catch (IllegalBlockSizeException e)
        {
            e.printStackTrace();
        }
        catch (BadPaddingException e)
        {
            e.printStackTrace();
        }
        catch (InvalidKeyException e)
        {
            e.printStackTrace();
        }
        catch (InvalidParameterSpecException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public String toString()
    {
        return "PeerHelloResponse{" +
                "encryptedData=" + encryptedData + "\n" +
                ", encryptedAesKey=" + Arrays.toString(encryptedAesKey) + "\n" +
                ", responderDHPublicKey=" + Arrays.toString(responderDHPublicKey) + "\n" +
                ", initiatorChallengeNonceResponse=" + Arrays.toString(initiatorChallengeNonceResponse) + "\n" +
                ", responderChallengeNonce=" + Arrays.toString(responderChallengeNonce) + "\n" +
                '}';
    }
}
