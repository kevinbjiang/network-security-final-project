package edu.neu.kevinjiang.netsec.core.protocol.login;

import javax.crypto.spec.DHParameterSpec;
import java.io.Serializable;
import java.security.PublicKey;

/**
 * Created by kevinbjiang on 7/26/14.
 */
public class LoginHello implements Serializable
{
    public byte[] challengeNonce;
    public byte[] dhClientPublicKey;
}
