package edu.neu.kevinjiang.netsec.core;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by kevinbjiang on 7/27/14.
 */
public class DigestUtil
{
    public byte[] sha1(byte[] input)
    {
        MessageDigest md = null;
        try
        {
            md = MessageDigest.getInstance("SHA-1");
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new RuntimeException(e);
        }

        return md.digest(input);
    }
}
