package edu.neu.kevinjiang.netsec.core.protocol;

import edu.neu.kevinjiang.netsec.core.SymmetricCryptoEncryptResult;
import edu.neu.kevinjiang.netsec.core.SymmetricCryptoHelper;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import java.io.*;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidParameterSpecException;

/**
 * Created by kevinbjiang on 7/28/14.
 */
public class MessageEnvelope
{
    private static final SymmetricCryptoHelper symmetricCryptoHelper = new SymmetricCryptoHelper();
    public boolean encrypted = false;
    public Object message = null;
    public SymmetricCryptoEncryptResult encryptedData = null;

    public MessageEnvelope()
    {
    }

    public MessageEnvelope(Serializable message, SecretKey encryptionKey) throws IOException, NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException, InvalidParameterSpecException
    {
        if (message == null)
        {
            throw new RuntimeException("Can't set null object");
        }

        //if key provided encrypt byte array
        if (encryptionKey != null)
        {
            this.encrypted = true;

            //serialize to byte array
            byte[] objectBytes = null;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bos);

            oos.writeObject(message);
            oos.flush();
            bos.flush();

            byte[] data = bos.toByteArray();

            this.encryptedData = symmetricCryptoHelper.encrypt(data, encryptionKey);
        }
        else
        {
            this.message = message;
        }
    }

    public Object read(SecretKey decryptionKey)
    {
        if (encrypted)
        {
            try
            {
                //decrypt byte array
                byte[] decrypedBytes = symmetricCryptoHelper.decrypt(encryptedData.getCipherText(), encryptedData.getIv(), decryptionKey);

                //deserialize decrypted byte array
                ByteArrayInputStream bis = new ByteArrayInputStream(decrypedBytes);
                ObjectInputStream ois = new ObjectInputStream(bis);
                Object o = ois.readObject();
                return o;
            }
            catch (NoSuchPaddingException e)
            {
                e.printStackTrace();
            }
            catch (NoSuchAlgorithmException e)
            {
                e.printStackTrace();
            }
            catch (InvalidAlgorithmParameterException e)
            {
                e.printStackTrace();
            }
            catch (InvalidKeyException e)
            {
                e.printStackTrace();
            }
            catch (BadPaddingException e)
            {
                e.printStackTrace();
            }
            catch (IllegalBlockSizeException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            catch (ClassNotFoundException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            return message;
        }

        return null;
    }

}
