package edu.neu.kevinjiang.netsec.core.protocol.peer;

import java.io.Serializable;

/**
 * Created by kevinbjiang on 7/28/14.
 */
public class PeerMessage implements Serializable
{
    private long messageid;
    private String message;

    public PeerMessage()
    {
    }

    public PeerMessage(long messageid, String message)
    {
        this.messageid = messageid;
        this.message = message;
    }

    public long getMessageid()
    {
        return messageid;
    }

    public String getMessage()
    {
        return message;
    }
}
