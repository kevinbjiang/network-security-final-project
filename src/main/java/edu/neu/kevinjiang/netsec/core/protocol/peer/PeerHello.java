package edu.neu.kevinjiang.netsec.core.protocol.peer;

import edu.neu.kevinjiang.netsec.core.AsymmetricCryptoHelper;
import edu.neu.kevinjiang.netsec.core.JavaSerializer;
import edu.neu.kevinjiang.netsec.core.SymmetricCryptoEncryptResult;
import edu.neu.kevinjiang.netsec.core.SymmetricCryptoHelper;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.*;
import java.security.spec.InvalidParameterSpecException;
import java.util.Arrays;

/**
 * Created by kevinbjiang on 7/26/14.
 */
public class PeerHello implements Serializable
{
    final private static AsymmetricCryptoHelper asymmetricCryptoHelper = new AsymmetricCryptoHelper();
    final private static SymmetricCryptoHelper symmetricCryptoHelper = new SymmetricCryptoHelper();
    final private static JavaSerializer javaSerializer = new JavaSerializer();

    private SymmetricCryptoEncryptResult encryptedData;
    private byte[] encryptedAesKey;

    //transient
    private transient String username;
    private transient byte[] initiatorChallengeNonce;
    private transient byte[] initiatorDHPublicKey;

    public PeerHello()
    {
    }

    public PeerHello(String username, byte[] initiatorChallengeNonce, byte[] initiatorDHPublicKey)
    {
        this.username = username;
        this.initiatorChallengeNonce = initiatorChallengeNonce;
        this.initiatorDHPublicKey = initiatorDHPublicKey;
    }

    public byte[] getInitiatorDHPublicKey()
    {
        return initiatorDHPublicKey;
    }

    public byte[] getInitiatorChallengeNonce()
    {
        return initiatorChallengeNonce;
    }

    public String getUsername()
    {
        return username;
    }

    public void decrypt(PrivateKey key)
    {
        try
        {
            if (encryptedData != null && encryptedAesKey != null)
            {
                //decrypt aes key
                byte[] decryptedAesBytes = asymmetricCryptoHelper.decrypt(key, encryptedAesKey);
                SecretKey aesKey = new SecretKeySpec(decryptedAesBytes, "AES");

                //decrypt data
                byte[] decryptedBytes = symmetricCryptoHelper.decrypt(encryptedData.getCipherText(), encryptedData.getIv(), aesKey);

                //deserialize data
                ByteArrayInputStream bis = new ByteArrayInputStream(decryptedBytes);
                try (ObjectInputStream ois = new ObjectInputStream(bis))
                {
                    username = (String) ois.readObject();
                    initiatorChallengeNonce = (byte[]) ois.readObject();
                    initiatorDHPublicKey = (byte[]) ois.readObject();
                }
                catch (ClassNotFoundException e)
                {
                    e.printStackTrace();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
        catch (NoSuchPaddingException e)
        {
            e.printStackTrace();
        }
        catch (InvalidKeyException e)
        {
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        catch (IllegalBlockSizeException e)
        {
            e.printStackTrace();
        }
        catch (BadPaddingException e)
        {
            e.printStackTrace();
        }
        catch (InvalidAlgorithmParameterException e)
        {
            e.printStackTrace();
        }

    }

    public void encrypt(PublicKey key)
    {
        try
        {
            if (initiatorChallengeNonce != null && initiatorDHPublicKey != null)
            {
                //generate aes key
                SecretKey aesKey = symmetricCryptoHelper.generateRandomKey();

                //encrypt aes key
                byte[] aesKeyBytes = aesKey.getEncoded();
                byte[] encryptedAesKeyBytes = asymmetricCryptoHelper.encrypt(key, aesKeyBytes);
                this.encryptedAesKey = encryptedAesKeyBytes;

                //encrypt data with aes key
                byte[] objectsBytes = null;
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                try (ObjectOutputStream oos = new ObjectOutputStream(bos))
                {
                    oos.writeObject(username);
                    oos.writeObject(initiatorChallengeNonce);
                    oos.writeObject(initiatorDHPublicKey);
                    oos.flush();
                    bos.flush();

                    objectsBytes = bos.toByteArray();
                    encryptedData = symmetricCryptoHelper.encrypt(objectsBytes, aesKey);
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
        catch (NoSuchPaddingException e)
        {
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        catch (IllegalBlockSizeException e)
        {
            e.printStackTrace();
        }
        catch (BadPaddingException e)
        {
            e.printStackTrace();
        }
        catch (InvalidKeyException e)
        {
            e.printStackTrace();
        }
        catch (InvalidParameterSpecException e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public String toString()
    {
        return "PeerHello{" +
                "encryptedData=" + encryptedData + "\n" +
                ", encryptedAesKey=" + Arrays.toString(encryptedAesKey) + "\n" +
                ", username='" + username + '\'' + "\n" +
                ", initiatorChallengeNonce=" + Arrays.toString(initiatorChallengeNonce) + "\n" +
                ", initiatorDHPublicKey=" + Arrays.toString(initiatorDHPublicKey) + "\n" +
                '}';
    }
}
