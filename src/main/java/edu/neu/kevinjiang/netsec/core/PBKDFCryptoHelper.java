package edu.neu.kevinjiang.netsec.core;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

/**
 * Created by kevinbjiang on 7/29/14.
 */
public class PBKDFCryptoHelper
{
    public byte[] deriveKeyFromPassword(String password, byte[] salt)
    {
        try
        {
            SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            KeySpec ks = new PBEKeySpec(password.toCharArray(), salt, 1024, 128);
            SecretKey s = f.generateSecret(ks);
            return s.getEncoded();
        }
        catch (InvalidKeySpecException e)
        {
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
