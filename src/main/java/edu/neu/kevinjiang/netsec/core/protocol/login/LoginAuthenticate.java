package edu.neu.kevinjiang.netsec.core.protocol.login;

import java.io.Serializable;
import java.security.PublicKey;

/**
 * Created by kevinbjiang on 7/26/14.
 */
public class LoginAuthenticate implements Serializable
{
    public String username;
    public String password;
    public int port;
}
