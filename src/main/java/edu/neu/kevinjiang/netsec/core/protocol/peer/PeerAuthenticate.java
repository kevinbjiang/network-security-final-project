package edu.neu.kevinjiang.netsec.core.protocol.peer;

import java.io.Serializable;

/**
 * Created by kevinbjiang on 7/26/14.
 */
public class PeerAuthenticate implements Serializable
{
    private byte[] responderChallengeNonceResponse;
    private String initialMessage;

    public PeerAuthenticate()
    {
    }

    public PeerAuthenticate(byte[] responderChallengeNonceResponse, String initialMessage)
    {
        this.responderChallengeNonceResponse = responderChallengeNonceResponse;
        this.initialMessage = initialMessage;
    }

    public byte[] getResponderChallengeNonceResponse()
    {
        return responderChallengeNonceResponse;
    }

    public String getInitialMessage()
    {
        return initialMessage;
    }
}
