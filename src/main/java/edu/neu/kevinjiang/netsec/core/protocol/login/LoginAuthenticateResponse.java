package edu.neu.kevinjiang.netsec.core.protocol.login;

import java.io.Serializable;
import java.security.PrivateKey;

/**
 * Created by kevinbjiang on 7/26/14.
 */
public class LoginAuthenticateResponse implements Serializable
{
    public boolean authenticated;
    public PrivateKey sessionPrivateKey;
}
