package edu.neu.kevinjiang.netsec.core.protocol.session;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by kevinbjiang on 7/26/14.
 */
public class ListResponse implements Serializable
{
    public long inResponseTo;
    public Set<String> users;

    @Override
    public String toString()
    {
        return "ListResponse{" +
                "inResponseTo=" + inResponseTo +
                ", users=" + users +
                '}';
    }
}
