package edu.neu.kevinjiang.netsec.core.protocol.session;

import java.io.Serializable;

/**
 * Created by kevinbjiang on 7/26/14.
 */
public class ClientInfoRequest implements Serializable
{
    public long messageId;
    public String username;
}
