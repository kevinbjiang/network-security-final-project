package edu.neu.kevinjiang.netsec.core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by kevinbjiang on 8/1/14.
 */
public class PropertiesReader
{
    protected Properties properties = new Properties();
    public PropertiesReader(String filePath)
    {
        try (FileInputStream fileInputStream = new FileInputStream(filePath))
        {
            properties.load(fileInputStream);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public Properties getProperties()
    {
        return properties;
    }
}
