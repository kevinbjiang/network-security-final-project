package edu.neu.kevinjiang.netsec.core.protocol.session;

import java.io.Serializable;

/**
 * Created by kevinbjiang on 7/26/14.
 */
public class HeartbeatResponse implements Serializable
{
    public long inResponseTo;
}
